import React, {Component} from 'react';
import ReactDOM from 'react-dom';

export default class First1 extends Component {
    state = {
        counts : 0
    };
    
    constructor(){
        super();
//        this.Increase = this.Increase.bind(this);
//        this.handleMouseOver = this.handleMouseOver.bind(this);
    }
    
    Increase = (e) => {
        
        this.setState({counts : this.state.counts + 1});
        console.log(this.state)
      
    };
    
    handleMouseOver = (e) => {
        
         console.log(e.target, e.pageX)
    }
 
    handleCopy = (e) => {
        
        console.log("hello")
    }
    
    handleChange = (e) => {
        
        this.setState({
            name: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }
    
    render() {
        
        return (
            <div>
                <button onClick={this.Increase}>Increment</button>
                <h1>{this.state.counts}</h1>
                
                <button onMouseOver={this.handleMouseOver}>Hover Me</button>
                
                <p onCopy={this.handleCopy}>Hello, ready to be copied.</p>
                
                <form onSubmit={this.handleSubmit}>
                    <h1>{this.state.name}</h1>
                    <br/>
                    <input type ="text" value= {this.state.name} onChange={this.handleChange}/>
                    <button>Submit</button>
                    <br/>
                    
                </form>
                
            </div>
        );
    }

   
}
