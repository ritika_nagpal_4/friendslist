import React, {Component} from 'react';
//import ReactDOM from 'react-dom';

function Paginator(items, page, per_page) {
 
  var page = page || 1,
  per_page = per_page || 10,
  offset = (page - 1) * per_page,
 
  paginatedItems = items.slice(offset).slice(0, per_page),
  total_pages = Math.ceil(items.length / per_page);
  return {
  page: page,
  per_page: per_page,
  pre_page: page - 1 ? page - 1 : null,
  next_page: (total_pages > page) ? page + 1 : null,
  total: items.length,
  total_pages: total_pages,
  data: paginatedItems
  };
}

export default class FriendsList extends Component {
    
     state = {
        data: [ 
         {Name : 'Theodore Roosevelt', bookmark : true, imageLogo: "https://img.icons8.com/ios-filled/50/000000/star.png", sex : "Male"},
         {Name : 'Abraham Lincoln', bookmark : false, imageLogo: "https://img.icons8.com/ios/50/000000/star.png", sex : "Male"}, 
         {Name : 'George Washington', bookmark : false, imageLogo: "https://img.icons8.com/ios/50/000000/star.png", sex : "Male"}, 
        ],
         imageUrl : "https://img.icons8.com/ios-filled/50/000000/star.png" 
    };
    
    constructor(){
        super();

    }
    
    bookUnbook = (e) => {
    
         if(this.state.data[e.target.id].bookmark){
             let newState = Object.assign({}, this.state);
             newState.data[e.target.id].imageLogo = "https://img.icons8.com/ios/50/000000/star.png";
             newState.data[e.target.id].bookmark = false
            this.setState(newState);
             console.log(this.state)
         }
            else{
            let newState = Object.assign({}, this.state);
            newState.data[e.target.id].imageLogo = "https://img.icons8.com/ios-filled/50/000000/star.png";
            newState.data[e.target.id].bookmark = true
            this.setState(newState);
                console.log(this.state)
                
            }
  
    }
    
    delete = (e) => {
    
        let newState1 = Object.assign({}, this.state);
         newState1.data.splice([e.target.id],1)
         this.setState(newState1);
        alert('An entry has been deleted successfully')
        console.log(this.state)
        
    }
    
   handleInput = (e) => {
//       console.log(e.which)
       if(e.which === 13){
           if(e.target.value.length > 2){
               var txt;
                var gender = prompt("Please enter your Gender", "");

                    if (gender == null || gender == "") {
                      alert("Please enter a valid value");
                    } else {
                        
                        if (gender == "male" || gender == "female" || gender == "Male" || gender == "Female" )  {     
                                txt = gender;
                                console.log(txt)
                                let newState2 = Object.assign({}, this.state);
                                 const nameCapitalized = e.target.value.charAt(0).toUpperCase() + e.target.value.slice(1)
                                 newState2.data.push({Name:nameCapitalized, bookmark: false, imageLogo: "https://img.icons8.com/ios/50/000000/star.png", sex:txt})
                                 console.log(newState2)
                                 this.setState(newState2);
                        } 
                        else{
                            alert('Please enter a valid value')
                        }
                 }
           }
           else{
               alert('Please enter a valid name')
           }
       }
   }
    
    render() {
        
        var items = []
        for(let i = 0; i < this.state.data.length; i++){
            console.log(Paginator(this.state.data));
            items.push(<li>
            <div className="bookMark col-md-4 ">{this.state.data[i].Name} <br/>
                   <span className="badge badge-info">{this.state.data[i].sex}</span>
                    </div>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                <div className="bookMark col-md-2 col-xs-2 form-control">
                    <img src={this.state.data[i].imageLogo} id={i} onClick={this.bookUnbook} className="form-control" />
                </div>
                <div className="bookMark col-md-2 col-xs-2 form-control">
                    <img src="https://img.icons8.com/metro/52/000000/trash.png" className="form-control" id={i} onClick={this.delete}/>
                </div>
            </li>)
        }
        
        return (
            
            
            <div id="myUL">
              <div className="container">
                  <div className="col-md-6 col-xs-6 col-lg-6"></div>
                  <div className="col-md-4 col-xs-4 col-lg-4 ">
                       <h3>The FriendList</h3>
                   </div>
                <div className="col-md-2"></div>    
            </div>
               
                <br></br>
                <ul>
                    <div className="container">
                       <div className="col-md-6 col-xs-6 col-lg-6"></div>
                       <div className="col-md-4 col-xs-4 col-lg-4">
                            <input type="text" className="form-control" id="example1" onKeyPress={this.handleInput} placeholder="Type the name of a friend" />
                            {items}
                        </div>
                        <div className="col-md-2"></div> 
                        
                        <br/>
                        <div className="col-md-6 col-xs-6 col-lg-6"></div>
                        <div className="col-md-4 col-xs-4 col-lg-4">
                            <h6>{this.state.data.length} entries found</h6>
                        </div>
                    </div>
                </ul>
                
            </div>
        );
    }
    
}